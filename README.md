# Developing and Testing an Asynchronous API with FastAPI and Pytest

Follow this course on https://testdriven.io/blog/fastapi-crud/ if you want to learn these objectives:

* Develop an asynchronous RESTful API with Python and FastAPI
* Practice Test-Driven Development
* Test a FastAPI app with Pytest
* Interact with a Postgres database asynchronously
* Containerize FastAPI and Postgres inside a Docker container
* Parameterize test functions and mock functionality in tests with Pytest
* Document a RESTful API with Swagger/OpenAPI